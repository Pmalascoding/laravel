<?php


Route::get('/table',function(){
    return view('layout.table');
});
Route::get('/data-tables',function(){
    return view('layout.data-table');
});
Route::get('/','HomeController@home');
Route::get('/daftar','AuthController@Register');
Route::post('/kirim','AuthController@Welcome');

Route::get('/cast/create','castController@create');
Route::post('/cast','castController@store');
Route::get('/cast','castController@index');
// show
Route::get('/cast/{cast_id}','castController@show');
// edit
Route::get('/cast/{cast_id}/edit','castController@edit');
// update
Route::put('/cast/{cast_id}','castController@update');
// delete
Route::delete('/cast/{cast_id}','castController@destroy');