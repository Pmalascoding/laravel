<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('register');
    }
    public function Welcome(Request $request){
        $nama = $request['nama'];
        $nama_belakang = $request['nama_belakang'];

        return view('welcome',compact('nama','nama_belakang'));

    }
}
