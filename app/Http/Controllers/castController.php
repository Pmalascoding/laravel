<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create(){
        return view('cast.create');
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Silahkan isi Data',
            'bio.required'  => 'Silahkan Isi Data',
        ]
    );
     
    DB::table('cast')->insert(
        [
            'nama' => $request['nama'],
            'bio' => $request['bio']
        ]
    );
        return redirect('/cast');
    }
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index',compact('cast'));
    }
    public function show($id){
        $cast = DB::table('cast')->where('id',$id) -> first();
        return view('cast.show',compact('cast'));
    }
    public function edit($id){
        $cast = DB::table('cast')->where('id',$id) -> first();
        return view('cast.edit',compact('cast'));
    }
    public function update($id, Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Silahkan isi Data',
            'bio.required'  => 'Silahkan Isi Data',
        ]
    );
    DB::table('cast')->where('id',$id)->update(
        [
            'nama' =>$request['nama'],
            'bio' =>$request['bio'],
        ]);
     return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id','=',$id) ->delete();
        return redirect('/cast');
    }
}
