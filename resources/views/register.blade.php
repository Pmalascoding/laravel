@extends('layout.master')
@section('judul')

@endsection
@section('content')
        <h1>Buat Account Baru</h1>
        <h3>Sign Up Form</h3>
        <form action="/kirim" method="post">
            @csrf
            <label for="nama">First Name :</label><br><br>
            <input type="text" name="nama" id="nama" ><br><br>
            <label for="nama">Last Name :</label><br><br>
            <input type="text" name="nama_belakang" id="nama" ><br><br>

            <label for="">Gender</label required><br><br>
            <input type="radio" name="jk" value="1">Laki Laki<br>
            <input type="radio" name="jk" value="2">Perempuan<br><br>

            <label for="nation">Nationality</label><br><br>
            <select name="pilih" id="nation">
                <option value="Indonesia">Indonesia</option>
                <option value="Russia">Russia</option>
                <option value="Turki">Turki</option>
            </select><br><br>

            <label for="lang">Languange Spoken</label><br>
            <input type="checkbox" name="idn" id="lang">Bahasa Indonesia<br>
            <input type="checkbox" name="en" id="lang">English<br>
            <input type="checkbox" name="en" id="lang">Arab<br>
            <input type="checkbox" name="other" id="lang">Japanese<br><br>

            <p>Bio</p>
            <textarea name="message" id="pesan" cols="30" rows="10"></textarea><br>
            <input type="submit" value="Sign Up">
        </form>

@endsection